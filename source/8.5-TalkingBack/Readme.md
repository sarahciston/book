---
Title: 8.5 Talking back
page_order: 8.5
---

{::options parse_block_html="true" parse_span_html="true" /}

![flowchart](ch8_5.svg) 
<!-- TK: MAKE FLOWCHART -->

[TOC]

## setup()

Almost as long as computers have existed, people have been talking to them and trying to give them the power of speech. From Christopher Strachey using the Mark I to generate love letters[^Strachey] to the many virtual assistants that we interact with throughout our day, computers are our synthetic conversants. They don’t need to pass the Turing Test. We don’t mind if they are obviously operating artificial. Consider the Chess-playing Turk or other early automaton.ß But the desire persists to create something, a la Victor Frankenstein, that can speak on its own. Or is the reference ultimately pygmalion, as is nicely illustrated in Emily Short’s interactive fiction *Galatea*? In this chapter we look at a few of the ways we create automated conversants, how our synthetic interlocutors become entangled in gender, sexuality, race and ethnicity, and socio-economic status or class, as well as how these tools might be used to respond to the systems in which they are embedded. 

This dream of the conversing machine takes various manifestations. Virtual assistants like Alexa and Siri are prominent examples, along with customer service phone chat popups becoming ubiquitous. But the UR-chatbot is *ELIZA*, discussed further in chapter 10. *ELIZA* can be considered the first chatbot and as the first model it introduces ancillary themes into the family tree of conversational agents. Built by Joseph Weizenbaum, the code has recently been made available.[^ElizaCode] What most people consider *ELIZA* is the program playing through the DOCTOR script, which follows the conversational patterns of Rogerian psychotherapy. Deceptively, it does not actually offer analysis, but instead elicits insights from the "patient" through its responses. In the DOCTOR script, ELIZA maintains conversational coherence by storing and revisiting keywords, asking follow-up questions that are broad enough to encourage possible connections. 

Conversation is a profoundly human form of connection, of collaborative making with other humans—though we talk to lots of nonhumans as well, be they plants or pets, vehicles or appliances. Software-based conversants are a profoundly post-human form of connection with machines, but they are no less a means of relational identity formation. When we turn conversation into an algorithmic process, we reveal the conditioned scripts and habits of speech that drive our interactions with one another. We also show that we can gain something from a discussion even when one conversation partner is a machine. Consider the way commonly used (and often despised) phone menus operate to either help clients get the information they desire or to keep them away from humans whose time needs to be protected. Or consider the way Twitter bots complement or add noise to a social media sphere where large networks of people merely retweet or repost talking posts and slogans. Perhaps software conversants are best when seen as a means to examine our empathy, how we treat others and form or break connections through dialog. The virtual agent shows conversation—symbolic exchange—to be a space for creative intervention.   

However, turning conversation into a machinic system had just as much an impact on ELIZA's descendants as its other human features: namely gender and socio-economic status. *ELIZA* comes into being as DOCTOR with a gender within the namesake context (*Pygmalion*) in which language is a system of imitation, of gendered performance, and of the negotiation of socio-economic status. Is ELIZA a female bot "taught" to speak like a man? Does DOCTOR's correct speech create a superior interlocutor who has knowledge of our internal motivations? ELIZA initiates computational dialogue as a space of gendered and classed negotiation with the power dynamics of person-to-person interaction. ELIZA's legacy continues to explore these dynamics.

<!-- I like how setup() is coming into shape as setting up the history of what Eliza staged culturally for all other chatbots and systems in general, and the new conclusion makes that explicit. I'm not sure it's easy enough to parse throughout the last paragraph–particularly the questions in the middle since readers don't yet have (or need) too much of the context that they'll get later in the chapter. I wonder if there's a way to distill this point down some? -->
<!--I added a temp conclusion to this section that keeps the emphasis on the social -->
<!-- need footnotes for items added above -->

## start()

*ladymouth*[^lm] is a chatbot that tries to explain feminism to misogynists online. Created in 2015 as a performance art project by Sarah Ciston to demonstrate the emotional labor and risk of interacting online for anyone perceived as feminine, or queer, or not white, or "othered," it visits subreddits to address self-identified "incels" and "men's rights activists" on behalf of feminists and feminine-presenting people who feel endangered by engaging them directly. The bot uses the Reddit API (application programming interfaces are introduced in chapter 8) to search for relevant keywords in posts, and then it comments with quotations from feminist scholars like bell hooks and Gloria Anzaldúa. Finally, it logs any responses it receives, which get incorporated into writing, live performance, and video art.

![lm](LMfigure-bot.jpg){: .medium}

*Figure 8.5.1: A reply to ladymouth’s participation in the Men’s Rights subreddit (reddit.com/r/mensrights).*

![lm](LMfigure-ban.jpg){: .medium}

*Figure 8.5.2: ladymouth participates in The Red Pill subreddit (reddit.com/r/theredpill), reacting to the keyword "SMV" or "sexual market value" from the subreddit's public glossary, and another Red Pill user responds.*

Because the bot inserts itself where it is an unwelcome, provocation, spectacle, and failure are part of its design. This style of artwork joins a long history of net art and tactical media.[^Raley] Here, the goal is not to eliminate misogyny, which is pervasive online and off,[^Jane] but to draw attention to the ways digital language impacts embodied physical subjects through an absurd exercise. *ladymouth* makes herself a target, distributing the risks of technology back onto a technological body, while wasting nano-increments of misogynists' time. Where historically women*[^wm] have been asked to care for and act as machines, *ladymouth* imagines a machine that can care for women*, multiply their effort, and deflect their abuse.[^lm]

<div class="section exercise" markdown="true">

## Code confessions

Learning to code can be plagued with many false start()s. The the not-so-secret origin story of *ladymouth* is that it was the *first ever* "real" program I successfully completed. This was after years—*years*—of trying to learn Javascript and JQuery and Processing (before P5.js even existed) and just not connecting to the lessons, thinking I was simply not "meant" to be a coder because nothing stuck. 

But I was lucky enough to have a professor[^Brett] push me to "try learning some Python by making something." It was the most terrifying class assignment, and I went in convinced I would fail. But because the goal was to pick something small—a new but tiny challenge, task, tool I could get excited about—I was able push past that feeling of intimidation and make incremental progress. 

Rather than comprehensively learning the entire coding language from a textbook, I learned I could apply a scrappier, more artistic approach: Explore the materials, play, break a project into its smallest parts, and web search the heck out of it. This may feel too silly to be "real" coding, but that's OKAY. You don't have to feel like a real programmer to be one. You already are!

— Sarah Ciston

</div>

## Pythonic potential

The following source code is a snippet from *ladymouth* focusing on how it pulls quotations from a [sqlite]() <!--explain--> database and uses a simplified version of the Reddit API (a library called a wrapper) to search for and post those quotations. 

But first: *"Where did all my curly braces and semicolons go!?"* you might be asking with some alarm. Why would the trusted authors spring a completely different programming language on you just as you're getting your feet wet with Javascript? 

Just like learning a new spoken language can help you better understand the grammar and syntax of the one(s) you already know well, we have found that experimenting with reading and writing in another programming language helps solidify the fundamental concepts of coding. You may even find you already understand more than you expect. Since the fundamental concepts of coding carry across almost all executable languages, seeing them expressed in new language can help reinforce the key ideas that are agnostic to the dialect—such as variables and data structures, loops, calculable expressions, and conditional statements.  
<!-- e.g. calculating, categorizing, storing, and looping ...see while... what am i forgetting? would be great to connect this to while subtly-->

Here are some examples of syntax that makes Python different from Javascript and other languages—what its fans sometimes call "pythonic":

* **Meaningful Indents and White Space**: Rather than curly braces and brackets, Python strives for a clear, readable approach by using indentation to indicate which lines are inside or outside the scope of each function. So keep that code looking tidy!
* **Not a 'Typed' Language**: When you make a new variable or object, you don't have to declare what type of data it is. That means no `var const int float` etc. With a few exceptions you can just say `x = 1` here. 
* **Simplified Syntax**: Python is praised for its simple syntax that some argue comes closer to natural English than most other programming languages. Many operations in Python can be reduced to their most efficient and succinct versions using techniques like list comprehensions[^list] and powerful built-in functions. It's an extreme sport among Python enthusiasts.
* **XXX ADDITIONAL THING XXX**: 
<!-- add a fourth item? -->

[^list]: `newList = [x for x in fruits if "a" in x]` Say what? A [list comprehension](https://www.w3schools.com/python/python_lists_comprehension.asp) makes a new list from an old list by plucking out only the requested items, all in one terse line of programmatic prose. 

<!-- * eg `loadJSON()`:[^]  -->


## Source code

```python
class Quote():

    def __init__(self, quoteID, author, body, tag):
        self.qid = quoteID
        self.author = author
        self.body = body
        self.tag = tag
 
    @staticmethod
    def pick_random():
        global q
        query = "SELECT * FROM quotations ORDER BY RANDOM() LIMIT 1"
        for row in sql.query(query):
            quoteID, author, body, tag = row #assigns sql output to the variables to pass to the class
        q = Quote(quoteID, author, body, tag) #sends variables to create object from class
        #should all these be separated into their own method?
        return q
 
    @staticmethod
    def pick_tag(tag):
        global q
        query = "SELECT * from quotations WHERE tag=?"
        for row in sql.query(query, (tag,)):
            quoteID, author, body, tag = row  #replace with ??? Quote.map_variables()
        q = Quote(quoteID, author, body, tag) #sends variables to create object from class
        return q
 
    @staticmethod
    def pick_ID(quoteID):
        global q
        query = "SELECT * FROM quotations WHERE quoteID=?"
        for row in sql.query(query, (quoteID,)):
            quoteID, author, body, tag = row
        q = Quote(quoteID, author, body, tag)
        return q
 
    #create a method that finds the last posted reply, pulls the next quoteID from it.
    @staticmethod
    def findlastused():
        query = "SELECT quoteID FROM replied ORDER BY quoteID DESC LIMIT 1;"
        for row in sql.query(query):
            lastID = row #NEED TO REMOVE FROM TUPLE HERE
        quoteID = int(lastID) + 1
        Quote.pickID(quoteID)
 
    def display(self):
        print(self.body + ' (' + self.tag + ')')
 
class Post():[^post]<sup>1</sup>
    def __init__(self, pid, author, body, q):
        self.pid = pid
        self.author = author
        self.body = body
        self.q = q #binds the quote object to the post
        self.reply = self.q.body + ' -' + self.q.author
 
    def display(self):
        print(self.author + ' says: ' + self.body)
        print('Would reply: ' + self.reply)
 
    @staticmethod
    def get():
        # make posts global to save the string and not have to redo the search?
        posts = r.get_subreddit(SUBREDDIT).get_comments(limit = LIMIT)
        posts = praw.helpers.flatten_tree(posts)
        return posts
           
    # HOW TO RETURN MORE THAN ONE MATCH??????
    @staticmethod
    def match_random(q):
        global match
        posts = Post.get()
        tag = q.tag
        matches = [post for post in posts if tag.lower() in post.body.lower()]
        if matches: # to make multiple: for match in matches, try reply_with
            match = random.choice(matches)
            Post.create(match)
            return match
        else:
            print('No matches found. Try again.')
            exit() # kill program here or try again?
            # would this be in a while loop? ADD A HANDLER
            #Quote.pick_random()
            #Post.match_random(q)
 
    # trying a new version that breaks up the steps into separate functions, tries returning multiple matches
    @staticmethod
    def match(q):
        posts = Post.get()
        tag = q.tag
        matches = [post for post in posts if tag.lower() in post.body.lower()]
        return matches
 
    @staticmethod
    def try_all():
        matches = Post.match(q)
        matchlist = list()
        if matches:
            for match in matches: #try reply with and log
                item = Post.create(match) # won't this just write over each version?, make a list of variables here to assign to each, or dispense with objects altogether
                matchlist.append(item) #how to assign different names to each
            return False
        else: #how to rerun, put in while loop instead?
            return True
 
    @staticmethod
    def create(m):
        global p
        pid = str(m.id)
        author = str(m.author)
        body = str(m.body)
        p = Post(pid, author, body, q)
        return p
 
    def check(self):
        sql.query("CREATE TABLE IF NOT EXISTS replied(pid TEXT, author TEXT, body TEXT, quoteID INTEGER)")
        query = "SELECT * FROM replied WHERE pid=?"
        sql.query(query, (self.pid,))
        already = sql.cur.fetchone()
        if already:
            print('Already replied to this post.')
            return True
        else:
            #print('Could reply.')
            return False
     
    def reply_with(self):
        if (self.check() is False) and (self.author != USER):  #checks that author is not bot--does this work?
            try:
                #UNCOMMENT TO GO LIVE
                #match.reply(self.reply)
                print('REPLIED: ' + self.reply)
                print('TO THIS POST: ' + str(match.body)) #check
                #log post -- UNCOMMENT TO GO LIVE
                #try:
                    #self.log()
                #except:
                    #print('Replied but could not log.')
            except:
                print('Could not reply.')
        else:
            print('Already replied or self-reply')
```




## Code commentary 

* Made using the [PRAW](https://praw.readthedocs.io/en/latest/) Python wrapper for the Reddit API

* Code Commentary:  To expand the scope of *Aesthetic Programming*, we are adding these sections of Code Commentary in order to emphasize the way code accrues meaning and to underscore the importance of exploring and explicating that meaning.  <!--Maybe cite Critical Code Studies here :) -->

We offer this section of code as an example of a chatbot that does not simulate the back and forth of everyday conversational exchange but instead the asynchronous exchange on a discussion forum.  Although the timing may make it appear different, the underlying logic is the same. 

This code excerpt from the *ladymouth* primarily does 3 things: it chooses the quotation from the database of feminist writings, files away the reddit post to which it is responding, and posts a reply on reddit. 

Also, it is important to note that this code represents an iterative exploration of a process.  For example, there are various methods for choosing a quote in the code; however, the *ladymouth* system makes use primarily of the keyword approach. We share this to encourage coders to experiment and to remind code readers that the code they see bears traces of the processs of its evolution, since coding is not an inscriptive process of writing down a finished idea but an instead a process of discovery, revision, and refactoring. 

```class Quote():```
This excerpt begins with class "Quote," which is the section that selects from the ladymouth database of feminist quotations. That (SQL-lite) database was built through a python file called database.py. In this file Quote is a class that interacts with the database and includes all of the methods that is needed for the chatbot to select a quote from the database. Again, some of these were used primarily during development. 

The selection functions choose a responses based on a random quote (pick_random), based on a keyword (pick_tag), and based on an ID (pick_ID) -- a unique identifier in the database -- used to select specific quotations. Each of the methods, runs a query to the database that is the quoted text and it gathers those columns of the database (quoteID, body, tag). @staticmethod tells the program that the method beneath it does not have to create an object.

```query = "SELECT * FROM quotations ORDER BY RANDOM() LIMIT 1"```
 Notice how legible the SQL code is:
   `query = "SELECT * from quotations WHERE tag=?"`

 SELECT (or get) * (everything) from quotations (the name of the table) WHERE tag=? (tag equals the tag in the argument of the method). <!-- possibly a note about the legibility of SQL --> pick_tag was the method used the most often in ladymouth, which makes builds ladymouth on the principal of keyword matching discussed throughout this chapter. 

The display method prints what the author, body and reply would be. This is another remnant of Sarah's development process for *ladymouth* as the display is primarily for the programmer and does not affect the system's interaction with the REDDIT forum.

 The database does 3 things: saves all the feminist quotations, saves all the original posts that it responds to, and saves all the replies that it gets to its own posts.  

```Post```
 Post(c) is the main class whose job is to interact with the reddit forum.  It finds posts that exists, and it creates new posts. Gives a post an id, author, body (the text of the post), attaches the quote to the post, and creates the reply. The author is the original post author, body is the text of their post, q is the quote from the database, and reply is bound to the quote from the database plus the author. 


 ```def get():```
 Uses the python wrapper for the reddit API to grab the most recent LIMIT number of posts. SUBREDDIT determines which forum it searches.  LIMIT helps the program follow the rules of pinging REDDIT by not grabbing too many comments and flatten_tree aggregates the comments (since it discussions on reddit forums exist in a tree, and equalizes them so that the program does not just grab the posts, which are the top level of the reddit hierarchy, but the comments as well).

 <!-- Possible sidebar on APIs and wrappers -->

 ```def match_random(q):```
 first takes the results of get() and compares it to the tag that we are using. To do so, it then converts all the text to lower case, looks for tag, then makes a new list of posts that correspond to that tag, calling that new list "matches."  If it finds a match, it tries to make a random choice from the matches list and moves on to one of the other functions to create its own post.  If it doesn't find any, it exits the operation and waits a bit to try again.  match was an earlier version of the method.

[ expanding on code annotations TK ]

Line 67/1[^quote] <!--how to show internal line system of code block-->

<!-- [^quote]: This class contains different approaches for selecting items from the database of feminist quotations based on random, matching keywords, recent, or id. -->
<!-- [^post]: This class contains all the actions related to searching for keywords in a forum and matching it to a post, then submitting a new post using the API.  -->

<div class="section exercise" markdown="true">

## Code confessions

When I wrote my first chatbot, it was an adaptation of a JavaScript implementation of *ELIZA*. (Okay, this does not count the BASIC chatbots I made as a youth.) I don’t think I had any idea what the JavaScript was doing. I just deleted the keywords and responses and replaced them with different data. Not knowing what the code did, it was easy to break. Plus, you know, JavaScript. So, I was constantly seeking help to find the quotation marks I forgot to close or the semicolon I omitted. (This still happens to me, but at least I know why.) Eventually, I started experimenting with other parts of the code, like the response delay. This experience won me over to a kind of beginning programming assignment, like the ones throughout this book, where you have the chance to write your own message in someone else's "Hello, World" so to speak. 

— Mark Marino

</div>

<div class="section exercise" markdown="true">

## Exercise in class

Take a look at this "cheezy little Eliza knock-off" by Joe Strout, Jeff Epler, and Jez Higgins.[^Strout] It operates along the basic principles of Weizenbaum’s DOCTOR, largely responding to input with formulaic replies.  <!--section needs footnotes for all the items added above-->

* Make some changes to the keywords it responds to.
  
* Make some changes to the responses it gives.

Discuss with your group: 

* How do your changes affect the experience of interacting with the script, both in terms of users' feelings and their impression of the conversational agent?

* What kinds of changes did you make and to what parts of the script? How were they similar and different from your peers? What motivated your choices?

Discuss with the class:

* This chatbot follows loosely the structure of Weizenbaum's DOCTOR script. Rather than developing a rich sense of the patient or drawing some corpus of language to learn how to hold a dialogue, the code uses a comparatively simple, but surprisingly effective, method of searching for keywords and then responding in a pattern-based way. This doctor seems to be following a version of Rogerian psychotherapy. However, what makes this bot effective is the power of mirroring statements in this type of counseling and, as Noah Wardrip-Fruin (in _Expressive Processing_)[^NWF] has noted, people's tendency to attribute consciousness to surface appearances of intelligence. 

* Much of computer programming involves breaking a complex process, such as conversation, into a systematic process. However, there are many types of conversation that even between humans that follows set patterns. Think of knock-knock jokes, courtroom appeals, even greetings. <!-- JL Austin performativity could go here but not needed--> With this patterning in mind, how are your everyday conversations predictable and where do they go "off script"? What other systematized interactions can you imagine scripting like the ELIZA Doctor system?

</div>

## Eliza and her daughters

ELIZA is an <!--un?-->usual bot. No sooner was she made available than people started confessing their secrets to her. Or should I say "him," since when she acted as DOCTOR, following the script for a therapist, interactors gendered the bot male, as in the case of the young woman who likens DOCTOR to her father in Weizenbaum’s sample transcript. What we call ELIZA is a DOCTOR script playing out on the ELIZA system, a program in MAD-SLIP (a LISP-like language[^MADlanguage]) whose code recently was made public.[^ELIZACode] The way people used ELIZA concerned its creator, Joseph Weizenbaum, who had witnessed during the Holocaust the dark turns technology could take if unchecked, how easy it was to attribute or deny humanity to machines or to certain types of humans.

Subsequent systems would return to this categorization. Consider for example, Peggy Weill's *Mr. Mind* in her *Blurring Test*, a chatbot system that asks humans to prove that they are human. Or the early chatbot Julia that Sherry Turkle writes about in _Life on the Screen_.[^Turkle] Her gender was a constant subject of conversation. Lai-Tze Fan has written about Alexa and the way this female-presenting system is designed to respond in servile ways to male abuse and quips.[^Fan] Alexa is a virtual assistant gendered female and is subject to abuse in ways that the programmers are well aware. 

But whatever we make out of ELIZA or DOCTOR, we are constructing that perception out of very little. The original *ELIZA* has a very simple keyword-based response mechanism. It searches for particular words, which then trigger responses. Nonetheless, digital poet Nick Montfort has called it one of the most impactful programs, since it has inspired so much creative response, ports, emulations, reimaginings, and evolutions.[^Montfort] 

<!--section needs footnotes for all the items added above-->

## Source code

Consider this excerpt from the DOCTOR script. <!--is it Doctor or DOCTOR?-->

```python

#----------------------------------------------------------------------
# gPats, the main response table.  Each element of the list is a
#  two-element list; the first is a regexp, and the second is a
#  list of possible responses, with group-macros labelled as
#  %1, %2, etc.
#----------------------------------------------------------------------

gPats = [
  [r'I need (.*)',
  [  "Why do you need %1?",
    "Would it really help you to get %1?",
    "Are you sure you need %1?"]],

  [r'I want (.*)',
  [  "What would it mean to you if you got %1?",
    "Why do you want %1?",
    "What would you do if you got %1?",
    "If you got %1, then what would you do?"]],

  [r'(.*) mother(.*)',
  [  "Tell me more about your mother.",
    "What was your relationship with your mother like?",
    "How do you feel about your mother?",
    "How does this relate to your feelings today?",
    "Good family relations are important."]],

  [r'(.*) father(.*)',
  [  "Tell me more about your father.",
    "How did your father make you feel?",
    "How do you feel about your father?",
    "Does your relationship with your father relate to your feelings today?",
    "Do you have trouble showing affection with your family?"]],
```

<div class="section exercise" markdown="true">

## Code commentary: Keywords 

This code excerpt primarily shows the simple exchange the bot uses to produce conversation, which follows the model of the original DOCTOR script. As the code comment explains, each element in the list has two components: a regular expression (regex) that it searches for and a list of possible responses. Note that these are called lists in Python, whereas in another language they would be considered arrays inside objects.

This short excerpt demonstrates two kinds of patterns to match. The first kind looks for the particular "I" kind of statement from the interlocutor, an "I want" or an "I need" statement. For this pattern, the code does not need to know the object of the declaration, but merely can reply by inverting that statement into a follow-up question using the object of the first statement in the subsequent question. 

The second kind of response looks for the specific nouns "mother" and "father" and then responds in kind to these specific triggers or "events." <!--intents?--> In these search fields, we can identify the world model of the conversation system. People will express wants and needs that are worth interrogating, and mothers and fathers are particularly important topics to discuss at length. <!--Even a passing familiarity with psychoanalysis (or lived human experience) will support the emphasis on relationships with parents to personal well being.--> Arguably, the chatbot's attention to these particular terms offer one way the system seems to achieve something like insight. But it can also reveal how these choices of language reflect the specific social contexts in which the program is situated.

For example, this code makes more of "father" and "mother" than the original ELIZA code, which translates FATHER, MOTHER, WIFE, CHILDREN, BROTHER, SISTER as FAMILY and then treats those accordingly, with perhaps some priorities revealed in the order of the family members.

```
(MOTHER DLIST(/NOUN FAMILY))
(MOM = MOTHER DLIST(/FAMILY))
(DAD = FATHER DLIST(/FAMILY))
(FATHER DLIST(/NOUN FAMILY))
(SISTER DLIST(/FAMILY))
(BROTHER DLIST(/FAMILY))
(WIFE DLIST(/FAMILY))
(CHILDREN DLIST(/FAMILY))
(FATHER DLIST(/NOUN FAMILY))
```

In other words, though not functionally, this version of the code devotes more lines of code to particular family members than Weizenbaum's code did, which could emphasize these distinct roles more to readers of the code. Since these roles are gendered, it increases the amount of gendered knowledge handling in the code and underscores again the efficiency of Weizenbaum's code. What we imagine to be the assumptions of Weizenbaum's conversation system (such as the existence of gendered epistemologies) are even at the code level so often our own projections. 

</div>

## Building conversational agents

*ELIZA* is a powerful program whose influence is bigger than the sum of its parts. At its core, the DOCTOR script merely searches for keywords in the input and responds to them in formulaic ways.  

Both Eliza and *ladymouth* work by using keywords. Note how this code does the same scanning procedure that *ladymouth* does, but the relationship to conversation is different. For ELIZA, the keywords are inputs that are stored for later use in conversation. ELIZA as DOCTOR, like a good Rogerian therapist, uses the input from the interactor to keep the conversation going. Meanwhile, *ladymouth*'s keywords identify particular posts so that it can write back from its database of predetermined quotations. In many ways, it is offering a response for comments that invite and deserve none. The former is responding to a tennis serve with a thoughtful yet forceful slam, the latter is trying to keep the volley going. 
<!-- If *ladymouth* makes conversation by scanning data, with the DOCTOR the interactor creates conversation by providing data in response to the DOCTOR's questions. DOCTOR is all queries. *ladymouth* by contrast is all replies.  -->

In both cases, mechanical conversation is a process of cause and effect, rather than, say understanding. What’s key here is not that we are creating smart bots, but taking an extremely clear contextualized interaction, such as natural language conversation, and emulating it using surface characteristics that are not digested so much as operationalized.  

Like *ladymouth*, the core of *ELIZA* is a search for a keyword, or regular expression (regex). Regex is perhaps most commonly used in simple validation, an operation which might seem the opposite of sophisticated conversational simulation. This is merely confirming that input matches the appropriate format. However, this simple tool can easily create an illusion of exchange.

Contemporary conversation programs make use of regex, as well as sentiment analysis, text generation, and various other machine learning techniques. As conversational agents grow more complex, developers frame these systems in terms of utterances, intents, entities, and responses: 

* An **utterance** is the raw input, the specific words or symbols the interactor speaks or types.
* The **intent** is the overall request, goal, or category inferred from the utterance, usually an action to perform.
* **Entities** are modifiers that the system uses to classify the input further (sometimes called **slots**)—nouns and adjectives to help give more specific details to that action. 

If someone writes: *I want to know the price of purple cotton yarn yesterday.* The whole sentence is their original utterance. Their intent is to ask the price. And “purple cotton,” “yarn,” and “yesterday” are entities. 

With this information, the bot decides what response to offer, as well as any actions it needs to perform in order to do so—e.g. perform a search for the current price of the specific item. 

Contemporary systems, such as Amazon's ALEXA and [Lex Chatbot](https://docs.aws.amazon.com/lex/latest/dg/how-it-works.html), extend this system of keywords and regex by adding Natural Language Understanding (NLU) to derive intents from utterances based on more complex machine-learning-based pattern finding <!--some comprehension of meaning--> rather than straight matching. That means the system can "understand" or recognize input beyond a list of precise search strings. The principle is the same, but the implications for more naturalistic conversational exchange are profound since the programmer does not have to predict all possible input strings.

<div class="section exercise" markdown="true">

## Code confessions

It's 2015, and I don't even know yet how many more terrible things will rise to the surface. While *ladymouth* runs on a Raspberry Pi server in the other room, I cook dinner and I wonder how she is doing. I wonder who is calling her names. I worry that even with the added security of a disguised, isolated port on this separate server, she will be doxxed and connected to me. In some ways, this anthropomorphizing, empathizing, and worrying defeats her purpose, since she was meant to help me avoid all these worries. I was not meant to get attached to a bot. I did not intend to discover the worst of what was being said about me and others like me online, but I had to in order to build the bot. I needed to dig into the dregs of the Red Pill to write the keyword searches for the database, type each word and move it through my body. Because of the design of the database, I had to correspond each insulting keyword to each feminist quotation, matching them by "theme." I chose to read each and every comment she received in reply. The direct messages she received felt even more personal and violent, despite the fact that they were not technically addressed to me. Plus, as each subreddit kicked out *ladymouth* I had to find another of the no shortage of forums for her to visit. 

It's 2021, and I still have the worst of *ladymouth*'s DMs unintentionally committed to memory. Sometimes they pop into my head at random moments during the day. I think we carry codes with us in our bodies, and that's why I am interested in learning code and making work that can change these codes. 

— Sarah Ciston

</div>

## While()

Both *ladymouth* and *ELIZA* raise issues of gendered conversational exchange. In the case of *ELIZA*, gender is at play from its creation. *ELIZA* is software-as-woman taught to speak properly, whose gender shifts when she assumes the more authoritative role of DOCTOR. Although Weizenbaum did not signal this gender change or make gender the forefront of the program, gender emerges in his naming of the program and in the users' responses to the conversational agent—as empathetic or knowing, as ELIZA or DOCTOR—showing how gender is not only performative but relies on how that performance is interpreted. 

Of course, this gendering has ties in the Turing Test, Alan Turing's famed thought experiment that provides historical context for all conversation agents. In *Computing Machinery and Intelligence*, Turing proposes a game in which a computer tries to pass itself off as a human through conversation. <!-- A human could not pass for a machine, he offers, since a human cannot keep up with the speed of a machine in simple operations such as calculation. A computational machine, on the other hand, could imitate the types of responses a human would give. Hence, the imitation game.  --><!--interesting since so many Fordist and gig-econ jobs are in service of machine and fear of moving that direction-->However, this imitation game is a variation on a parlor game Turing describes in which a man tries to pass as a woman, while hidden behind a curtain and interacting only by passing notes. As the analogy goes, just as a man might pass himself off as a woman, a computer might pass itself off as a man through screened symbolic exchange. This gendered framework is inherited (to use a programming concept) by chatbots as they all take on some form of this challenge.

By this framing, performing as a human and performing as a woman both rely on specialized, cultural knowledge. Therefore, the job of a conversational agent becomes imitation, predicated on bridging a perceived difference, epitomized by gender difference. These imitation games at once produce, perform, and deconstruct that sense of difference. How we define gender and humanity are byproducts of symbolic exchange, read from and into symbols. Is it no surprise that these questions of gender re-emerge in our critical encounters with contemporary conversational systems such as Alexa and Siri, whose own stories are riddled with gendered and raced power dynamics, from their combative histories to the timbre of their voices. For example, before Siri was called "Siri," she was "CALO" and was designed as a soldier's servant for the US military, with a smarter, harder-edged personality to match.[^Finn]

The conversations around gender that arise from *ladymouth* take on a different tone from *ELIZA*, but it operates through many of the same underlying assumptions about performative gender. While it has been been accused of "trolling the trolls" and asked to be nicer, this critique also speaks to the gendered imperatives regarding who gets to speak and who must listen, who wields power forcefully and who submits gently. Interestingly, through the lens of misogyny (hatred of women*) or so-called misandry (hatred of men) discussed in the forums ladymouth visits, these performative, impossible ideals seem to end up hurting people of all genders. Their discussions are riddled with angry, hurting pleas for empathy and fair treatment, no matter how their framing those concepts differ. 

*ladymouth* triggers some of that empathy (and anger) across zones by recontextualizing those conversations into new spaces, whether putting feminist theory into the "manosphere" or its incel commentary into gallery installation, poetry performance, and academic conferences. It also offers a tactical media provocation about the labor of empathy. While *ELIZA* imitates the empathetic therapist, reflecting back what it "hears" in order to convince the user it listens, which is a form of (often gendered) emotional labor, *ladymouth* deflects the emotional labor of standing up for women* while protecting them from directly absorbing vitriol. It uses computation's ability sort and calculate at high scales and speeds in order to multiply and distribute that labor. Putting a machine in place of a person puts into relief the empathy vacuum that exists for the experiences of marginalized communities existing online and off. 

*ladymouth* also reveals how our discourse becomes automatic: By responding algorithmically to replications of misogynistic discourse, *ladymouth* challenges and attempts to counteract the way the messaging of hate speach can replicate and multiply especially when unchecked. Humans who defy the viral spread of hate face retribution through doxxing and other practices of intimidation. In this context, *ladymouth* knows no fear and operates as a less vulnerable discourse agent. The fact that similar processes have been used by bots designed to spread misinformation and other political propaganda only shows the power of the form and the importance of examining impact and intent in evaluating these projects.

*ladymouth* is tactical media in the tradition of intersectional queer feminist media praxis.[^lm] Tactical media, according to Rita Raley, is media that prioritizes intervention, experimentation, and disturbance over a "consumable product." She calls it a "mutable category" that includes practices like digital hijacking, collaborative software, open-access labs, reverse engineering, and hactivism.[^Raley] Inspired by denial-of-service attacks and other cumulative gestures, *ladymouth*'s spam is made of micro-interventions, perhaps imperceptible compared to the volume of what Anastasia Salter and Bridget Blodgett refer to as toxic geek culture[^Salter], which also uses digital spaces to propagate itself exponentially. 
*ladymouth* reveals how our discourse becomes automatic: By responding automatically and algorithmically to replications of misogynistic discourse, *ladymouth* challenges and attempts to counteract the way the messaging of hate speech can replicate and multiply especially when unchecked. Humans who defy the viral spread of hate face retribution through doxxing and other practices of intimidation. In this context, *ladymouth* knows no fear and operates as a less vulnerable discourse agent. The fact that similar processes have been used by bots designed to spread misinformation and other political propaganda only shows the power of the form and the importance of examining impact and intent in evaluating these projects.

This toxic geek culture operates on keywords of its own. Terms like "Misandry, which until recently was used almost exclusively within the manosphere, functions as part of [its] common linguistic practice. This creates a sense of community across divergent subgroups, builds ties between individuals, and helps to solidify the ideological commitment of MRAs [(men's rights activists)] to oppose feminism."[^Marwick] *ladymouth* is particularly interested in how language focuses and distributes power–from its simple keyword searches to the way the internet enables neologisms like "incel" to travel and galvanize group think. Since *ELIZA*, we have seen how computational agents amplify this power through programming's specific processes: e.g. calculating, categorizing, storing, and looping—the very same processes we learn across any programming language. This language matters—both the language transmitted online by conversational agents and nameless avatars, and the code languages used to transmit it. *ladymouth* offers one example of a way to harness keywords for productive disruption of networks of hate. 

<div class="section exercise" markdown="true">

## MiniX: Build your own chatbot
In this little exercise, you will create your own custom chatbot. However, rather than building a simple keyword-response bot, you will be teaching a bot how to talk using a machine learning method and some training. Though not quite as predictable as a keyword method, this style of bot is less brittle. For this exercise, we're adapting a an exercise posted by Avinash Navalani [on DataCamp](https://www.datacamp.com/community/tutorials/building-a-chatbot-using-chatterbot). (Yes, our forked chapter continues its forking praxis!) 

First, put this code into a Jupyter Notebook or Google Colaboratory or just copy [our document](https://colab.research.google.com/drive/1LrO2e9Ic64g_zyzIwgbpT776WSKYL-8g?usp=sharing). Here is the code:

```python
!pip install chatterbot
!pip install chatterbot_corpus
# Importing chatterbot
from chatterbot import ChatBot
# Create object of ChatBot class
bot = ChatBot('TiredBot')  #replace with bot name
# Create object of ChatBot class with Storage Adapter
bot = ChatBot(
    'TiredBot', #replace with bot name
    storage_adapter='chatterbot.storage.SQLStorageAdapter',
    database_uri='sqlite:///database.sqlite3'
)
# Create object of ChatBot class with Logic Adapter
bot = ChatBot(
    'TiredBot',   #replace with bot name
    logic_adapters=[
        'chatterbot.logic.BestMatch'],
)
# Inport ListTrainer
from chatterbot.trainers import ListTrainer

trainer = ListTrainer(bot)

# Replace these with sample conversation exchanges.
trainer.train([
    'Hi',
    'Hey, there. Feeling tired?',
'I am tired',
    'Why not take a nap?',
    'I am.',
    'I bet.',
    'I have a headeache',
    'Perhaps you should step away from the screen.',
    'I am feeling anxious',
    'I cannot take away your anxiety, but I find deep breaths help',
    'I can\'t sleep.',
    'Yes, that can be hard.',
    'Yawn',
    'I know, I could use some rest!',
    'Can you help me sleep?',
    'If I tell you about sleep theory, that might help you sleep!',
    'Thank you.',
    'I hope you can rest soon!'
])

name = input("What's your name? ")
# write a custom greeting line or lines.
print("I'm TiredBot! Let's face it, we're all tired. You seem pretty tired, too.")
while True:
    request=input(name+':')
    if request=='Bye' or request =='bye':
        print('Bot: Bye')
        break
    else:
        response = bot.get_response(request)
        print('Bot:',response)
```

Next, customize the bot. You can change its name and its initial greeting.  Then, change the sample lines of dialogue.  Remember, this bot is not looking for keywords. It's trying to learn how conversations work. To train it, you need to write sample conversational exchanges.  These will be added to Chatterbots learning library, so that it can have a more customized conversation.

**Objectives:**

* To experiment with keywords, intents, entities/slots, and utterances. 
* To understand existing conversational patterns and explicitly utilize them toward a goal.
* To reflect critically on the influences of computation and culture on the perception of those language patterns. 
<!-- Test snippet: -->
<script src="https://gitlab.com/-/snippets/2152615.js"></script>
<embed type="text/html" src="https://gitlab.com/-/snippets/2153530.js" width="300" height="300"></embed>

**Additional inspiration:**

<!-- artists works with chatbot, offer 3 examples -->
<!-- Blurring test -->
<!-- Tracery bot -->
<!-- Galatea? -->

**Tasks (RunMe):**



### Inputs
* What role will your chatbot fill?
* What are the keywords it will listen for?
* What more general intents do you expect?
* What patterns of conversation do you expect.

Since, our sample chatbot uses machine learning, one way of training is to feed in sample conversations.  Otherwise, it merely pulls from the chatterbot training texts.

### Outputs
* How will the bot respond to these intents?
* What happens when a user's input or bot's output is unexpected?
* How will you repair when the conversation gets off track?
* How will you deal with abusive content?

### Alternative Exercise

Try building a chatbot based on Amazon Lex, following either the directions [here](https://cobusgreyling.medium.com/creating-a-chatbot-with-amazon-lex-and-aws-lambda-a72eb2e08598) or the project described by John Murray[^Murray] in [this assignment](https://jtm.io/computational-media-amazon-lex-2020/).
<!-- needs citation -->
<!--https://cobusgreyling.medium.com/a-comparison-of-nine-chatbot-environments-cc4af61f1151 -->



**Questions to consider (ReadMe):**

*  How do virtual agents draw out our sense of humanity through conversation?
*  What do these synthetic systems say about the systems of conversation in our everyday lives?
*  For which tasks is a conversational agent suited? Do we need a human to tell us what the temperature is outside? Do we need a therapist to have deep intention behind the script they are running? Do we need to respond personally to hateful speech? How do we decide in which contexts we require a human or a machine "behind the screen" (especially when that binary is continually contested)? 
*  In what ways are laboring humans already dehumanized? How do we treat those in service positions, such as cashiers or receptionists, like they are less than human? What are some of the ways algorithmic systems contribute to these reactions? 

<!-- Furthermore, to what degree does rote political speech remove reflection from conversation? To what degree do systems run interference human connection? How has loneliness become a default human condition that we try to minimize with machines.  -->
<!-- (Reading into What ELIZA does….Eliza Affect)
But what if you wanted to do the opposite, to write into a database. -->

</div>
 
## Required reading

<!-- add more required readings, articles over books? -->
* Turkle, S. (1997). Life on the Screen: Identity in the Age of the Internet (First Edition). Simon & Schuster.
* 

## Further reading

* Cassell, J. (2000). Embodied Conversational Agents. MIT Press.
* Jane, E. A. (2017). Misogyny Online: A Short (and Brutish) History. SAGE.
* Marino, M. C. (2006). I, Chatbot: The Gender and Race Performativity of Conversational Agents [PhD Thesis]. University of California, Riverside.
* Phillips, W. (2015). This Is Why We Can’t Have Nice Things. MIT Press.

## Notes

[^lm]: Ciston, Sarah. (2019). “ladymouth: Anti-Social-Media Art As Research.” Ada: A Journal of Gender, New Media, and Technology, No. 15. [10.5399/uo/ada.2019.15.5](https://adanewmedia.org/2019/02/issue15-ciston/)
[^Brett]: (Hi [Brett Stalbaum](https://visarts.ucsd.edu/people/faculty/brett-stalbaum.html), and thank you for teaching me python and pedagogy and courage!!) 
[^Jane]: Jane, E. A. (2017). Misogyny Online: A Short (and Brutish) History. SAGE.
[^NWF]: Wardrip-Fruin, N. (2019). Expressive Processing - Digital Fictions, Computer Games, and Software Studies. The MIT Press.
[^Marwick]: Marwick, A. E., & Caplan, R. (2018). Drinking male tears: Language, the manosphere, and networked harassment. Feminist Media Studies, 18(4), 543–559. https://doi.org/10.1080/14680777.2018.1450568
[^MADlanguage] Organick, E. I. (1961) The Computer Primer for the MAD Language. Ann Arbor, Mich., Photolithoprinted by Cushing-Malloy. https://babel.hathitrust.org/cgi/pt?id=mdp.39015021689271
[^ElizaCode]: https://sites.google.com/view/elizagen-org/the-original-eliza
[^Strachey]: "Darling Sweetheart / You are my avid fellow tiding. My affection curiously clings to your passionate wish. My liking yearns for your heart. You are my wistful sympathy: my tender liking. / Yours beautifully / M.U.C." (discussed further in ch 5). Strachey, The "Thinking" Machine. Encounter, October 1954, pp. 25-31. 
[^Raley]: Raley, R. (2009). Tactical Media. U of Minn. 
[^wm]:
[^Salter]:


<!-- However, the power of *ELIZA* is not so much in its operations as in its conceit. This systematic conversation software used comparatively simple programming operations to engage users in powerful ways. Also, this program inspired not just conversation agents, but non-player characters in games and other interactive systems.  -->

<!-- Programmers have made sophisticated systems by building on the keyword systems, even without using more complex tools like machine learning, by translating statements into speech acts in an adaptation of the work of J.L. Austin. Another use of this method can be found in the system that runs the interactive drama *Façade*, by Michael Mateas and Andrew Stern. In that conversational system, input is translated into one of 9 possible speech acts that affect the status of the system.  -->
