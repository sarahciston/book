# builds the database
# module can run in gui or command line
 
import sqlite3
import random
import csv
 
DATABASE = 'ladymouth.db'
 
# starts sql, loads database
sql = sqlite3.connect(DATABASE) #change to '/home/rpi/Documents/LadyMouthMachine/ladymouth.db'
cur = sql.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS quotations(quoteID INTEGER PRIMARY KEY, author TEXT, quote TEXT, tag TEXT)")
cur.execute("CREATE TABLE IF NOT EXISTS replied(pid TEXT, author TEXT, body TEXT, quoteID INTEGER)")
cur.execute("CREATE TABLE IF NOT EXISTS responses(pid TEXT, author TEXT, body TEXT, botid TEXT)")
cur.execute("CREATE TABLE IF NOT EXISTS lyric(lid INTEGER PRIMARY KEY, body TEXT)")
 
def add_manually():
    author = input('Author? ')
    quote = input('Quotation? ')
    tags = input('Tags? ')
    insert(author, quote, tags)
 
# takes user prompts from gui or add_manually
def insert(author, quote, tags):
    tags = tags.split(', ') #makes a list of tags
    for tag in tags:
        ins = "INSERT INTO quotations (author, quote, tag) VALUES(?, ?, ?)"
        cur.execute(ins, (author, quote, tag))
    sql.commit() #does this need to be in the loop? how many inserts can I queue up before committing?
    #return a boolean to confirm if it worked?
 
# FUNCTIONS BY TAG
#reads out all (tags) - could I make inheritance versions of this as a class to show all authors or quotations too?
def show_tags():
    cur.execute("SELECT DISTINCT tag FROM quotations")
    items = cur.fetchall()
    for item in items:
        item = ''.join(item)
        print(item)
 
# delete all quotes with a particular tag (could change to author or quoteID)
def delete_tag(tag):
    #tag = input('Remove quotes by tag? ')
    #tag = 'best'
    cur.execute("DELETE FROM quotations WHERE tag=?", (tag,))
    sql.commit()
    #show_tags() #bugcheck
 
# replace a tag that isn't working properly
def replace_tag():
    old_tag = input('Old tag? ')
    new_tag = input('New Tag? ')
    cur.execute("UPDATE quotations SET tag=? WHERE tag=?", (new_tag, old_tag))
    sql.commit()
    show_tags() #bugcheck
 
# searches quotations based on user input
def find_tag(tag):
    #tag = input('Find tag? ')
    search = ('%' + tag + '%',)
    query = "SELECT quote, author FROM quotations WHERE tag LIKE ?"
    cur.execute(query, search)
    items = cur.fetchall()
    if items:
        print('Found: ')
        for item in items:
            item = ' -'.join(item)
            print(item)            
            #print(' -'.join(item))
        #for quote, author in items:
            #print(str(quote) + ' -' + str(author))
    else:
        print('none')
    return items
 
def find_random(tag):
    query = "SELECT quote, author FROM quotations WHERE tag=? ORDER BY Random() LIMIT 1"
    cur.execute(query, (tag,))
    quote = cur.fetchall()
    print('Found: ')
    for q in quote:
        q = ' -'.join(q)
        print(q)
    else:
        print('none')
 
def show_quotes():
    query = "SELECT DISTINCT quote, author, tag FROM quotations"
    cur.execute(query)
    quotes = cur.fetchall()
    for q in quotes:
        q = ' -'.join(q)
        print(q)
 
 
# FUNCTIONS FOR IDs
def findID(ID):
    ID = (ID,)
    query = "SELECT quoteID, quote, author, tag FROM quotations WHERE quoteID=?"
    cur.execute(query, ID)
    item = cur.fetchall()
    print(item)
    return item
 
def deleteID(ID):
    cur.execute("DELETE FROM quotations WHERE quoteID=?", (ID,))
    sql.commit()
    show_quoteIDs() #bugcheck
 
def show_quoteIDs():
    cur.execute("SELECT quoteID FROM quotations")
    items = cur.fetchall()
    for item in items:
        print(item)
   
def randomizer():
    cur.execute("SELECT quote FROM quotations")
    items = cur.fetchall()
    items = [item for item in items]
    print(str(random.choice(items)))
 
 
#####
# FUNCTIONS FOR REPLIED DATABASE
def replied():
    cur.execute("SELECT quoteID FROM replied")
    items = cur.fetchall()
    '''if items:
       for item in items:
           print(item)
   else:
       print('none found')'''
    return items
 
# delete all quotes with a particular tag (could change to author or quoteID)
def delete_item(pid):
    cur.execute("DELETE FROM replied WHERE pid=?", (pid,))
    sql.commit()
    #replied() #bugcheck
 
# tracks how many times each quote is posted
def replycount():
    global replylist
    global badtags
    quoteIDs = replied()
    countlist = dict()
    replylist = dict()
    badtags = dict()
    x = range(0,100)
    for y in x:
        z = quoteIDs.count((y,))
        countlist[y] = z
    for key, item in countlist.items():
        if item != 0:
            replylist[key] = item
        if item == 0:
            badtags[key] = item
    return (replylist, badtags)
 
def badtags_print():
    replycount()
    for key in badtags.keys():
        findID(key)
   
def replylist_print():
    replycount()
    for key in replylist.keys():
        findID(key)
 
 
#####
# FUNCTIONS FOR RESPONSES DATABASE
def responses():
    cur.execute("CREATE TABLE IF NOT EXISTS responses(pid TEXT, author TEXT, body TEXT, botid TEXT)") #responseto = pid/ botID from replied table
    cur.execute("SELECT * FROM responses")
    items = cur.fetchall()
    if items:
        for item in items:
            print(item)
    else:
        print('none found')
 
 
#FUNCTIONS FOR PYGAME
def find_quote():
    query = "SELECT quote FROM quotations ORDER BY Random() LIMIT 1"
    cur.execute(query)
    item = cur.fetchall()
    for i in item:
        for t in i:
            t = ''.join(str(t))
    return t
 
def find_reply():
    query = "SELECT body FROM replied ORDER BY Random() LIMIT 1"
    cur.execute(query)
    item = cur.fetchall()
    for i in item:
        for t in i:
            t = ''.join(str(t))
    return t
 
def find_response():
    query = "SELECT body FROM responses ORDER BY Random() LIMIT 1"
    cur.execute(query)
    item = cur.fetchall()
    for i in item:
        for t in i:
            t = ''.join(str(t))
    return t
 
def find_lyric():
    query = "SELECT body FROM lyric ORDER BY Random() LIMIT 1"
    cur.execute(query)
    item = cur.fetchall()
    for i in item:
        for t in i:
            t = ''.join(str(t))
            t = t.replace('"', '').strip()
    return t
 
 
# export SQLite to CSV
def convertCSV():
    with open('ladymouth-responses.csv', 'wb') as csvfile:
        writer = csv.writer(csvfile)
        for row in cur.execute("SELECT * from responses"):
            writeRow = " ".join([str(i) for i in row])
            csvfile.write(writeRow.encode())
 
# LM lyric
# takes user prompts from gui or add_manually
def insert_lyric():
    body = input('Lyric? ')
    ins = "INSERT INTO lyric (body) VALUES(?)"
    cur.execute(ins, (body,))
    sql.commit()
 
# replace a tag that isn't working properly
def replace_lyric():
    old_lid = input('Old ID? ')
    new_body = input('New Text? ')
    cur.execute("UPDATE lyric SET body=? WHERE lid=?", (new_body, old_lid))
    sql.commit()
    show_lyric() #bugcheck
 
def show_lyric():
    query = "SELECT lid, body FROM lyric"
    cur.execute(query)
    quotes = cur.fetchall()
    for q in quotes:
        print(q)
 
# searches quotations based on user input
def find_keyword(tag):
    tag = input('Find tag? ')
    search = ('%' + tag + '%',)
    query = "SELECT body, lid FROM lyric WHERE body LIKE ?"
    cur.execute(query, search)
    items = cur.fetchall()
    if items:
        print('Found: ')
        for item in items:
            item = ' -'.join(item)
            print(item)            
            #print(' -'.join(item))
        #for quote, author in items:
            #print(str(quote) + ' -' + str(author))
    else:
        print('none')
    return items
